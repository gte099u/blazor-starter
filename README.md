# Blazor Starter project
Blazor-Starter is a quick-start project to get your Blazor apps up and running quickly along with some commonly used patterns and services.  Blazor-starter works out of the box with:
## Blazor Web App
* Blazor client-side web application - Dockerized and hosted at localhost:8080
* Blazorstrap styling included for ready-to-use Bootstrap 4.1 support
* Nginx reverse-proxy for serving the Blazor app and providing CORS for API calls
* A sample call to a public API with CORS support
## MVC API
* Dotnetcore MVC API with sample controller - Dockerized and hosted at localhost:8082
## Shared Model
* Example of leveraging shared Models (Blazor.Model) across API and Web service layers
## Sql Server Database
* Sql Server 2017 instance running in Linux Docker container.
* Initialized with single database, dbBlazorStarter
* Flyway migrations configured and running to initialize the dbBlazorStarter database to the latest migration on ```docker-compose up```
## Docker Build Analysis
* Analyze your Docker build contents to ensure ```.dockerignore``` is excluding files as desired

```
docker build -f Dockerfile.build-context -t build-context .
docker run --rm -it build-context
```

# Quick Start Instructions
Use ```docker-compose``` to start up the project. From the project root, first build the images,

```
docker-compose build
```

Then fire up the containers,

```
docker-compose up
```

Confirm that the services are up and running

[http://localhost:8080](http://localhost:8080) - Blazor Application should be running in the browser

[http://localhost:8082/api/customer](http://localhost:8082/api/customer) - Sample API response

Use your preferred SQL client to connect to the database at ```localhost:1433```  - you should see one database, ```dbBlazorStarter```

