PRINT 'Determining if dbBlazorStarter already exists...'
if not exists(select * from sys.databases where name = 'dbBlazorStarter')
begin
	PRINT 'Creating dbBlazorStarter'
    create database dbBlazorStarter
end
else
begin
	PRINT 'dbBlazorStarter already exists'
end