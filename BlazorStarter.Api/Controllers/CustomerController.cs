using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using BlazorStarter.Model;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        [HttpGet()]
        public IEnumerable<Customer> Get()
        {
            return new Customer[] {
                new Customer {
                    Name = "Daniel O",
                    CountryCode = "US"
                },
                new Customer {
                    Name = "Test User",
                    CountryCode = "UK"
                },
                new Customer {
                    Name = "Someone Else",
                    CountryCode = "FR"
                }
            };
        }

        [HttpGet("{id}")]
        public Customer Get(int id)
        {
            return new Customer
            {
                Name = "Someone Else",
                CountryCode = "FR"
            };
        }
    }
}