namespace BlazorStarter.Model
{
    public class Customer
    {
        public string Name { get; set;}
        public string CountryCode { get; set; }
    }
}
